package com.nannapat.midterm;

public class Trapper {
    private String name;
    private String type;
    private String perk1;
    private String perk2;
    private String perk3;
    private String power;

    public Trapper(String name, String type, String perk1, String perk2, String perk3, String power) {
        this.name = name;
        this.type = type;
        this.perk1 = perk1;
        this.perk2 = perk2;
        this.perk3 = perk3;
        this.power = power;
    }

    public void printTrapper() {
        System.out.println("Trapper");
        System.out.println("Name : " + name);
        System.out.println("Type : " + type);
        System.out.println("Perk1 : " + perk1);
        System.out.println("Perk2 : " + perk2);
        System.out.println("Perk3 : " + perk3);
        System.out.println("Power : " + power);
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getPerk1() {
        return perk1;
    }

    public String getPerk2() {
        return perk2;
    }

    public String getPerk3() {
        return perk3;
    }

    public String getPower() {
        return power;
    }
}
