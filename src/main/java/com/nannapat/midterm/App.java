package com.nannapat.midterm;

import java.util.Scanner;
import java.util.jar.Attributes.Name;

public class App {
    public static void main(String[] args) {
        Survivor survivor = new Survivor("Kate Denson", "Survivor", "Dance with me", "Windows of Opportunity",
                "Boil Over");
        Trapper trapper = new Trapper("The Nemesis", "Trapper", "Pursuer", "Hysteria", "Oblivious", "Tyrant");
        survivor.printSurvivor();
        trapper.printTrapper();
    }
}
